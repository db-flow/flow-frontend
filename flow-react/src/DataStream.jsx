// import { Line } from 'react-chartjs-2';
import React from "react";
import DataStreamRow from "./DataStreamRow";
import { MDBDataTable } from 'mdbreact';

const DataStream = props => {
    let rows = [];
    
    props.transactions.forEach((transaction, index) => {
      const lowerCaseProductName = transaction.instrumentName.toLowerCase();
      const lowerCaseFilterText = props.searchDetails.filterText.toLowerCase();
      if (
        lowerCaseProductName.indexOf(lowerCaseFilterText) === -1 ||
        (!transaction.stocked && props.searchDetails.inStockOnly)
      ) {
        return;
      }
      // if (transaction.category !== lastCategory) {
      //   rows.push(
      //     <DataStreamRow
      //       category={transaction.category}
      //       key={transaction.category}
      //     />
      //   );
      //   lastCategory = transaction.category;
      // }
      rows.push(<DataStreamRow transaction={transaction} key={index} />);
    });
    return (
      <div className="scroll">
      <table >
        <thead>
          <tr>
            <th>Instrument Name</th>
            <th>Counterparty</th>
            <th>Price</th>
            <th>Type</th>
            <th>Quantity</th>
            <th>Time</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
      </div>
    );
  };
  
  export default DataStream;