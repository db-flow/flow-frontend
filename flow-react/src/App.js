import React, { useState, Component } from 'react';
import { useObservable } from 'rxjs-hooks';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import DataContainer from './components/DataContainer';
import LoginForm from './components/LoginForm';
import LoginContainer from './components/LoginContainer';
import NavBar from './components/NavBar';
import Button from './Button';


class App extends Component {

  constructor(props) {
    super();
    this.state = { buttonClicked: false };
  }

  changeBtnState = () => {
    this.setState (
      { buttonClicked: true });
  }

  render() {
    const startPage = (
      <div>
        <NavBar />
        <div className="row">
          <div className="col-4"></div>
          <div className="col-4 p-6">
            <button className="btn btn-primary btn-round" onClick={ this.changeBtnState }>Login to your account</button>
          </div>
          <div className="col-4"></div>
        </div>
      </div>
    );
    return (<div>{ this.state.buttonClicked ? <LoginContainer /> : startPage }</div>);
  }
}

export default App;


    // <Router>
    //     <div>
    //       <h2>Welcome to React Router Tutorial</h2>
    //       <nav className="navbar navbar-expand-lg navbar-light bg-light">
    //       <ul className="navbar-nav mr-auto">
    //         <li><Link to={'/'} className="nav-link"> First </Link></li>
    //         <li><Link to={'/second'} className="nav-link"> Second </Link></li>
    //       </ul>
    //       </nav>
    //       <hr />
    //       <Switch>
    //           <Route exact path='/' component={FilterableProductTable} />
    //           <Route exact path='/second' component={FilterableProductTable2} />
    //       </Switch>
    //     </div>
    //   </Router>
