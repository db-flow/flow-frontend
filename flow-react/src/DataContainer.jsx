import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import DataStream from "./DataStream";
import BuyAndSellTable from "./BuyAndSellTable";

import { TRANSACTIONS } from "./transactions";

const DataContainer = props => {
    return (
        <div class="row">
          <div class="col-6">
            
            <DataStream transactions={TRANSACTIONS} 
            scrollY
            maxHeight="200px"/>
          </div>
          <div class="col-6">
            <BuyAndSellTable />
          </div>
        </div>
    );
  };
  
  export default DataContainer;