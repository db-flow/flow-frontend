import React, { useState } from "react";
import { useObservable } from 'rxjs-hooks';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import DataStream from "./DataStream";
import BuyAndSellTable from "./BuyAndSellTable";
import { TRANSACTIONS } from "./transactions";

const stringObservable = Observable.create(observer => {
  const source = new EventSource("http://127.0.0.1:5000/data_stream");
  source.addEventListener('message', (messageEvent) => {
    console.log(messageEvent);
    observer.next(messageEvent.data);
  }, false);
});

function StreamData(props) {
  const [stringArray, setStringArray] = useState([]);

  useObservable(
    state =>
      stringObservable.pipe(
        withLatestFrom(state),
        map(([state]) => {
          let updatedStringArray = stringArray;
          updatedStringArray.unshift(state);
          if (updatedStringArray.length >= 50) {
            updatedStringArray.pop();
          }
          setStringArray(updatedStringArray);
          return state;
        })
      )
  );

  return (
    <>
      {stringArray ? stringArray.map((message, index) => <p key={index}>{message}</p>) : <p>Loading...</p>}
    </>
  );
}

const DataContainer = props => {
    return (
    //     <div className="row">
    //       <div className="col-3">
    //         <DataStream transactions={TRANSACTIONS} />
    //       </div>
    //       <div className="">
    //         <BuyAndSellTable />
    //       </div>
    //       <div> 
    //         <StreamData /> 
    //       </div>
    //     </div>

          <div className="col-12"> 
            <StreamData />
            {/* <DataStream transactions = { TRANSACTIONS } /> */}
          </div>
    );
  };
  
  export default DataContainer;
    