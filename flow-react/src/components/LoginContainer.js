import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import LoginForm from "./LoginForm";
import NavBar from "./NavBar";

const LoginContainer = props => {
    return (
      <>
      <NavBar />
      <div className="row bg-grey lighten-4">
        <div className="col-12 p-6">
          <div className="fronttext">Flow Data Stream Application</div>
            <LoginForm/>
          </div>
      </div>
      </>
    );
  };
  
  export default LoginContainer;