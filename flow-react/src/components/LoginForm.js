import React, { useState } from "react";
import Button from "./Button";
import LineChart from "./LineChart";
import Panel from "./Panel"
import DataContainer from "./DataContainer"
import axios from 'axios';
import FilterableTransactionTable from "./FilterableTransactionTable";

class LoginForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            status: "default",
            dbStatus: ""
        };
    }

    handleChangeUsername = event => {
        this.setState({
            'username': event.target.value  
        });
    }

    handleChangePassword = event => {
        this.setState({
            'password': event.target.value  
        });
    }

    sendHttpLoginRequest = async (loginJson) => {
        var baseUrl ='http://127.0.0.1:5000';
        var loginEndpoint = baseUrl + '/login';
        // send HTTP POST request to the '/login' endpoint and return the response
        const response = await axios.post(
            loginEndpoint,
            loginJson,
            { headers: { 'Content-Type': 'application/json' } }
        );
        return response;
    }

    sendHttpCheckDbStatusRequest = async () => {
        var baseUrl ='http://127.0.0.1:5000';
        var checkDbEndpoint = baseUrl + '/db_status';
        // send HTTP GET request to the '/db-status' endpoint and return the status
        const response = await axios.get(
            checkDbEndpoint
        );
        return response;
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        var jsonString = JSON.parse('{ \"username\": \"' + this.state.username + '\", \"password\": \"' +  this.state.password  + '\"}');
        var response = undefined;
        try {
            response = await this.sendHttpLoginRequest(jsonString);
            if(response.status !== "200") {
                throw new Error(response.status)
            }
            this.setState({ 'status': response.status });
        }
        catch (error) {
            console.log(error.message);
            if (error.message != "Network Error") {
                var tokens = error.message.split(" ")
                this.setState({ 'status': tokens[tokens.length - 1] });
            }
            else {
                this.setState({ 'status': "503" });
            }
        }
        const dbStatusResponse = await this.sendHttpCheckDbStatusRequest();
        const sts = (dbStatusResponse.data == "True") ? "connected" : "not connected";
        this.setState({ 'dbStatus': sts });
    }

    render() {
        if (this.state.status == "default" || this.state.status == "401" || this.state.status == "503") {
            return (
            <>
            <div className="row bg-grey lighten">
                <div className="col-4"></div>
                <div className="col-4 text-center">
                    <form onSubmit={this.handleSubmit}>
                        <label htmlFor="username" className="fieldlabel">Username</label>
                        <input type="text" name="username" className="field" onChange={this.handleChangeUsername} required></input>
                        <label htmlFor="username" className="fieldlabel pswrd">Password</label>
                        <input type="password" name="password" className="field" onChange={this.handleChangePassword} required></input>
                        { (this.state.status == "401") && <p className="text-red mt-2">Credentials invalid.</p> }
                        { (this.state.status != "default" && this.state.status != "401") && <p className="text-red mt-2">This service is unavailable at the moment.</p> }
                        <Button />
                    </form>
                </div>
                <div className="col-4"></div>
            </div>
            </>
            )
        }
        else if (this.state.status == "200") {
            return (
                <div className="w-100">
                    <>
                    <h5 className="mb-3">Database Connection Status: <code className="text-green">{ this.state.dbStatus }</code></h5>
                    <FilterableTransactionTable />
                    <LineChart />
                    </>
                </div>
            )
        }
    }
}

export default LoginForm;
  