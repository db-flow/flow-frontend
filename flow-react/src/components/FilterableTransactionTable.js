import React, { useState } from "react";
import { useObservable } from 'rxjs-hooks';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


// import ProductTable from "./ProductTable";
import DataStream from "./DataStream";
// import DataContainer from './DataContainer';

import SearchBar from "./SearchBar";

// import { PRODUCTS } from "./products";

// import { TRANSACTIONS } from "./transactions";
const stringObservable = Observable.create(observer => {
  const source = new EventSource("http://127.0.0.1:5000/data_stream");
  source.addEventListener('message', (messageEvent) => {
    console.log(messageEvent);
    observer.next(messageEvent.data);
  }, false);
});

const FilterableTransactionTable = props => {
  const [filterText, setFilterText] = useState(``);
  const [inStockOnly, setInStockOnly] = useState(false);
  const [ transactions, setTransactions ] = useState([]);

  useObservable(
    state =>
      stringObservable.pipe(
        withLatestFrom(state),
        map(([state]) => {
          let updatedTransactions = transactions;
          updatedTransactions.unshift(JSON.parse(state));
          if (updatedTransactions.length >= 20) {
            updatedTransactions.pop();
          }
          setTransactions(updatedTransactions);
          return state;
        })
      )
  );

  const handleFilterTextChange = changedFilterText => {
    setFilterText(changedFilterText);
  };

  const handleInStockOnlyChange = changedInStockOnly => {
    setInStockOnly(changedInStockOnly);
  };

  return (
    <div>
      <h2>Recent Deals</h2>
      <DataStream
        transactions = { transactions }
        searchDetails = { filterText }
        
      />
      <SearchBar
        searchDetails={{ filterText, inStockOnly }}
        handleFilterTextChange={handleFilterTextChange}
        handleInStockOnlyChange={handleInStockOnlyChange}
      />
    </div>
  );
};

export default FilterableTransactionTable;
