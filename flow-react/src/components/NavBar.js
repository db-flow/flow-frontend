import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";


const NavBar = props => {
  return (
    <div className="row bg-grey nav-shadow p-2">
        <a href="http://www.db.com">
            <img className="avatar" src={require("./logo2.jpg")} alt="db-logo" />
        </a>
    </div>
  );
};

export default NavBar;