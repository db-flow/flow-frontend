import React from "react";
import SubmitButton from "./SubmitButton";
import { TRANSACTIONS } from "./transactions";

const SearchBar = props => {
  return (
    <form>
      <label htmlFor="filter">Instrument: </label>
      <input
        type="text"
        placeholder="Search..."
        value={props.searchDetails.filterText}
        onChange={event => props.handleFilterTextChange(event.target.value)}
        className="formEntry"
        id="filter"
      />
      <label htmlFor="start">Start date:</label>
      <input type="datetime-local" id="start" name="range-start" className="formEntry"></input>
      <label htmlFor="start">End date:</label>
      <input type="datetime-local" id="end" name="range-end" className="formEntry"></input>
      <SubmitButton />
    </form>
  );
};

export default SearchBar;
