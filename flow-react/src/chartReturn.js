export const INSTRUMENTINFO = {
    "lineChart": [
      {
        "labels": [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ]
      },
      {
        "datasets": [
          {
            "label": "Average Buy Price",
            "borderColor": "rgba(255,99,132,1)",
            "borderWidth": 1,
            "hoverBackgroundColor": "rgba(255,99,132,0.4)",
            "hoverBorderColor": "rgba(255,99,132,1)",
            "data": [65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56]
          },
          {
            "label": "Average Sell Price",
            "borderColor": "rgba(99, 109, 255, 1)",
            "borderWidth": 1,
            "hoverBackgroundColor": "rgba(255,99,132,0.4)",
            "hoverBorderColor": "rgba(255,99,132,1)",
            "data": [22, 45, 60, 89, 14, 23, 67, 98, 11, 13, 24, 35]
          }
        ]
      }
    ]
};