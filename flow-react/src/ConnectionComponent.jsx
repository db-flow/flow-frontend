import React, {Component} from 'react';
import { CONNECTIVITY } from './databaseConnectivity';

export default class ConnectionComponent extends Component{
    render (){

        function evaluate(){
            if(CONNECTIVITY.connection == 0){
                return "Not Connected";
            }else if(CONNECTIVITY.connection == 1){
                return "Currently Connecting";
            }else if(CONNECTIVITY.connection == 2){
                return "Connected";
            }else{
                return "We apologize for this error, please contact support";
            }
        }

        function displayName(){
            if(CONNECTIVITY.connection == 2){
                return "Welcome, ".concat(CONNECTIVITY.name, " ");
            }
        }


        return(
            <>
            <h2>Database Status: {evaluate()} <br></br>{displayName()} </h2>
            {/* <h2></h2> */}
            </>
        );


    }
}