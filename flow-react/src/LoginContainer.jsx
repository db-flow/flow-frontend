import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import LoginForm from "./LoginForm";

const LoginContainer = props => {
    return (
        <div class="row">
          <div class="col-4"></div>
          <div class="col-4 p-6">
            <div class="fronttext">Log In</div>
              <LoginForm/>
            </div>
          <div class="col-4"></div>
        </div>
    );
  };
  
  export default LoginContainer;