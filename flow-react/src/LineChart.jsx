import React, {Component} from 'react';
import { Line } from 'react-chartjs-2';
import { INSTRUMENTINFO } from "./chartReturn";
import SubmitButton from "./SubmitButton";
import { TRANSACTIONS } from "./transactions";


const data = {
  labels: INSTRUMENTINFO.lineChart[0].labels,
  datasets: INSTRUMENTINFO.lineChart[1].datasets
};

export default class LineDemo extends Component {
  render() {

    return (
      <>
      <div>
        <h2>Buy And Sell Prices</h2>
        <Line ref="chart" data={data} />
      </div>
      <form>
        <label htmlFor="Asset">Instrument: </label>
        <select id="Asset" className="formEntry">
          <option value="Astronomica">Astronomica</option>
          <option value="Borealis">Borealis</option>
          <option value="Celestial">Celestial</option>
          <option value="Deuteronic">Deuteronic</option>
          <option value="Eclipse">Eclipse</option>
          <option value="Floral">Floral</option>
          <option value="Galactia">Galactia</option>
          <option value="Heliosphere">Heliosphere</option>
          <option value="Interstella">Interstella</option>
          <option value="Jupiter">Jupiter</option>
          <option value="Koronis">Koronis</option>
          <option value="Lunatic">Lunatic</option>
        </select>
      <label htmlFor="start" >Start date: </label>
      <input type="datetime-local" id="start" name="range-start" className="formEntry"></input>
      <label htmlFor="start">End date: </label>
      <input type="datetime-local" id="end" name="range-end" className="formEntry"></input>
      <SubmitButton />
      </form>
      </>
    );
  }
}

