import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchBar from './SearchBar';
import Container from './Container';
import Button from './Button';
import ProductTable from './ProductTable';
import { PRODUCTS } from './products';

const FilterableProductTable2 = props => {
  return (
    <div>
      <>
        <div class="row">
          <div class="col-4"></div>
          <div class="col-4 text-center p-6"><Game2/></div>
          <div class="col-4"></div>
        </div>
      </>
    </div>
    );
};

class MyBotton2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'LOG OUT',
      showMe: true
    };
  }
  
  render() {
    return (
      <button 
        className="square" 
        onClick={() => this.setState({
          value: 'OK',
          showMe: false}
        )}
      >
        {this.state.value}
      </button>
    );
  }
}

class Game2 extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <MyBotton2 />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

export default FilterableProductTable2;
