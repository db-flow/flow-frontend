import React from "react";

const DataStreamRow = props => {
    return <tr>
    <td>{props.transaction.instrumentName}</td>
    <td>{props.transaction.cpty}</td>
    <td>{props.transaction.price}</td>
    <td>{props.transaction.type}</td>
    <td>{props.transaction.quantity}</td>
    <td>{props.transaction.time}</td>
</tr>
};

export default DataStreamRow;