import React, { useState } from "react";

// import ProductTable from "./ProductTable";
import DataStream from "./DataStream";

import SearchBar from "./SearchBar";

// import { PRODUCTS } from "./products";

import { TRANSACTIONS } from "./transactions";

const FilterableTransactionTable = props => {
  const [filterText, setFilterText] = useState(``);
  const [inStockOnly, setInStockOnly] = useState(false);

  const handleFilterTextChange = changedFilterText => {
    setFilterText(changedFilterText);
  };

  const handleInStockOnlyChange = changedInStockOnly => {
    setInStockOnly(changedInStockOnly);
  };

  return (
    <div>
      <h2>Recent Deals</h2>
      <DataStream
        transactions={TRANSACTIONS}
        searchDetails={{ filterText }}
      />
      <SearchBar
        searchDetails={{ filterText, inStockOnly }}
        handleFilterTextChange={handleFilterTextChange}
        handleInStockOnlyChange={handleInStockOnlyChange}
      />
    </div>
  );
};

export default FilterableTransactionTable;
