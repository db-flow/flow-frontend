import React from "react";

const BuyAndSellTable = props => {
    return (
        <table>
            <tr>
                <th>Instrument</th>
                <th>Average Buy Price</th>
                <th>Average Sell Price</th>
            </tr>
            <tr>
                <td>Dummy1</td>
                <td>DummyBuy1</td>
                <td>DummySell1</td>

            </tr>
            <tr>
                <td>Dummy2</td>
                <td>DummyBuy2</td>
                <td>DummySell2</td>
            </tr>
            <tr>
                <td>Dummy3</td>
                <td>DummyBuy3</td>
                <td>DummySell3</td>
            </tr>

        </table>
    );
  };
  
  export default BuyAndSellTable;