import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchBar from './SearchBar';
import Container from './Container';
import Button from './Button';
import ProductTable from './ProductTable';
import { PRODUCTS } from './products';

const FilterableProductTable = props => {
  return (
    <div>
      <>
        <div class="row">
          <div class="col-4"></div>

          <div class="col-4 text-center p-6"><myBotton2/></div>
          <div class="col-4"></div>
        </div>
      </>
    </div>
    );
};

class myBotton2 extends React.Component {
  constructor(props) {
      super();
      this.state = {loadWorksheep: false};
  }

  loadStuff = () => {
      this.setState({loadWorksheep: true});
  }

  render() {
    const startPage = (
        <div className="myBotton2">
            <h1>Первая страничка</h1>
            <button onClick={this.loadStuff}>Начать</button>
        </div>
    );
  return (<div>{ this.state.loadWorksheep ? <WorksheetSelector/> : startPage }</div>);
  }
}

function WorksheetSelector(props) {
  return (
      <div>
          <h1>Выберите группы</h1>
          <button>Next step</button>
      </div>
  );
}

class MyBotton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'LOG IN'
    };
  }

  render() {
    return (
      <button
        className="square"
        onClick={() => this.setState({
          value: 'OK'
        }
        )}
      >
        {this.state.value}
      </button>
    );
  }
}

class Game extends React.Component {
  render() {
    return (
      <div className="game">
        <div className="game-board">
          <MyBotton />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

export default FilterableProductTable;
